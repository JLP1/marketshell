Forex-Market-Data-Parsing
=========================

This illustrates shell scripting for data parsing and formatted output, processing sample Forex data.

Example Shell commands include:

• cut
• tr
• sort
• uniq
• pr
• awk
• tput

The included PNG image displays samples of the output. The image is composited by photoshop to fit the different sections in a smaller frame for demo purposes.

![fx_process.png](https://bitbucket.org/repo/5oo8zb/images/1009013561-fx_process.png)