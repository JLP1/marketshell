#!

# Purpose: data processing of financial time series data - Foreign Currency Pairs - (FX, Forex)
# demonstrates various Bash Shell methods of data and text processing with formatted output.

# specify data file
FX_DATA="sample_fxdata_280912.txt"

# set interface colors - can be improved for performance
GREEN="tput setaf 2"
RED="tput setaf 1"
CYAN="tput setaf 6"
BLUE="tput setaf 4"
MAGENTA="tput setaf 5"
YELLOW="tput setaf 3"
NORMAL="tput sgr0"

SET_NORMAL=$(tput sgr0)
SET_GREEN=$(tput setaf 2)
SET_RED=$(tput setaf 1)

# section border
SECTION='...........................................................................'

# create a simple function for rending the section headers
function section_header {
    printf "\n%s\n%s\n\n" $SECTION "$1"   
}

# * * * Process Market Data Time Series: * * *

# COLUMN HEADERS -----------------------------------------
# output a formatted list of the column headers
section_header 'Forex data column headers:'

FX_DATA_HEADER=$(cat $FX_DATA | head -1 | tr -s '[<>,]' ' '  )

$MAGENTA
#cat $FX_DATA | head -1 | tr '>' ' ' | tr '<' ' ' | tr ',' '\n'  
cat $FX_DATA                          \
            | head -1                 \
            | tr -d '[<>]'            \
            | tr ',' '\n'             \
            | pr -o 5 -w 5 -n1 -l 10 
$NORMAL

# CURRENCY PAIRS -----------------------------------------
# output a formatted list of currency pairs in both pages view and columns
section_header 'Currency Pairs:'

echo "Shell output: ( Pages )"
$YELLOW
cat $FX_DATA                           \
            | tail -n +2               \
            | cut -d',' -f1            \
            | sort                     \
            | uniq                     \
            | pr -o 10 -w 5 -n2 -l 20 
$NORMAL
echo ""
echo "Shell output: ( Columns )"
echo ""
$YELLOW
cat $FX_DATA                          \
            | tail -n +2              \
            | cut -d',' -f1           \
            | sort                    \
            | uniq                    \
            | rs 6                    
$NORMAL

# HISTORICAL DATA -----------------------------------------
# output a formatted list of the last set of historical data
DATA=$( cat $FX_DATA | tail -20 | tr ',' ' ' )

section_header "Historical Data:"

header="    %-8s %-12s %-10s %-10s %-10s %-10s %-10s\n"
format="%10s    %-10s %-10s %-10s %-10s %-10s %-10s\n"
width=43
printf "$header" $FX_DATA_HEADER
$CYAN
printf "$format" $DATA
$NORMAL
echo ""

# LATEST PAIR RATES -----------------------------------------
# output a formatted list of the lates rates, pair name, high and low prices
section_header "Latest Rates:"

LATEST_PRICES=$(cat $FX_DATA               \
                | tail -n +2                 \
                | tr ',' ' '                 \
                | sort -n -t' ' -k3  -r      \
                | awk '!x[$1]++'             \
                | sort                       \
                | cut -d' ' -f1,5,6 )

printf "%10s $SET_GREEN%10s $SET_RED%10s$SET_NORMAL\n" "PAIR" "HIGH" "LOW"
echo ""
printf "%10s $SET_GREEN%10s $SET_RED%10s$SET_NORMAL\n" $LATEST_PRICES

printf "\ndone.\n"

